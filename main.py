from machine import Pin, I2C
import ssd1306
from time import sleep
from dht import DHT11


# ESP8266 Pin assignment Oled Display
i2c = I2C(scl=Pin(5), sda=Pin(4))

# ESP8266 Pin assignment DHT11
sensorDHT = DHT11(Pin(2))

while True:
    sleep(3)

    oled_width = 128
    oled_height = 64

    oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)

   # print("T={:02d} ºC, H={:02d} %".format(temp, hum))

    sensorDHT.measure()
    temp = sensorDHT.temperature()
    hum = sensorDHT.humidity()

    temperature_string = str(temp)
    humedad_string = str(hum)

    oled.text('Temperatura:', 0, 0)
    oled.text(temperature_string, 100, 0)
    oled.text('Humedad:', 0, 20)
    oled.text(humedad_string, 100, 20)

    oled.show()
